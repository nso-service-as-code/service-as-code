*** Settings ***
Library        REST
Library        OperatingSystem
Library        String

# Resource files

Suite Setup     RESTCONF Suite Setup Actions
Suite Teardown  RESTCONF Suite Teardown Actions

Variables    ../service.yaml

*** Variables ***

${nso_url}   http://test_nso:80/

${test_dir}   ../tests

*** Test Cases ***

# Check if the list of services known by NSO matches the services we indent to provide
Verify if expected L2VPN services exist in NSO

    FOR    ${key}    IN      @{sample}
        ${name}=   Evaluate    $key.get("name")
        GET    ${nso_url}restconf/data/sample:customer-id
        # Output
        ${service-name}=   String    $..collection."sample:customer-id"[*].sample[*].name
        Should Contain    ${service-name}   ${name}
    END

# Check if xconnect IP address configured on delivery-device matches the expected IP of the access device
Check devices configuration is as expected

    FOR    ${key}    IN      @{sample}
        ${name}=   Evaluate    $key.get("name")
        ${delivery-device}=   Evaluate    $key.get("delivery-device").get("device-id")
        ${interface}=         Evaluate    $key.get("delivery-device").get("interface-id")
        ${interface}=	      Replace String	${interface}	/	%2F
        ${expected-ip}=       Evaluate    $key.get("access-device").get("ip-address")
        # Log To Console    ${delivery-device}  ${interface}

        GET    ${nso_url}/restconf/data/tailf-ncs:devices/device=${delivery-device}/config/tailf-ned-cisco-ios:interface/GigabitEthernet=${interface}
        # Output
        ${xconnect-ip}=   String    $.."tailf-ned-cisco-ios:GigabitEthernet"[*].service.instance.[*].xconnect.address
        Should Contain   ${xconnect-ip}   ${expected-ip}
    END

# Show configuration applied
Show configuration applied
    ${output}=    GET    ${nso_url}restconf/data/sample:customer-id
    Log   ${output} 

*** Keywords ***

RESTCONF Suite Setup Actions
    Set HTTP Header
    Retrieve Rollback

RESTCONF Suite Teardown Actions
    Apply Rollback

Set HTTP Header
    Set Headers	{ "Authorization": "Basic YWRtaW46YWRtaW4="}
    Set Headers	{ "Accept": "application/vnd.yang.collection+json"}
    Set Headers	{ "Content-type": "application/yang-data+json"}

Retrieve Rollback
    GET    ${nso_url}/restconf/data/tailf-rollback:rollback-files
    # ${response}=  Rest.Output response body tailf-rollback:rollback-files file 0 fixed-number
    ${response}=  String    $..file[0].fixed-number
    # Log To Console   ${response}
    ${last_id}=  Convert To Integer   ${response}[0]
    Set Suite Variable    ${rollback_id}   ${last_id+1}
    # Log To Console   ${rollback_id}

Apply Rollback
    POST     ${nso_url}/restconf/data/tailf-rollback:rollback-files/apply-rollback-file   { "fixed-number": ${rollback_id} }
    # Number   response status  204
