#!/bin/bash

# Pull docker images
docker login registry.gitlab.com
docker pull registry.gitlab.com/nso-service-as-code/sample-service
docker pull gitlab/gitlab-runner:latest
