#!/bin/bash

while getopts u: flag
do
    case "${flag}" in
        u) username=${OPTARG};;
    esac
done

# Change USER variable on each workshop laptop to achieve separation of environmentts
export GLUSER=$username
export TOKEN=glrt-rcy-J3gDJ5ZGYY-yvKee

# Start your local docker-compose environment
docker-compose -f compose.yml up -d

# Verify if all containers are running. Give it some time for the NSO container to fully start
docker ps

# The gitlab-runner will be dynamic, meaning that each branch will be associated with specific runner
# We will create userX git branch and the pipeline will be executed on userX runner

# now register the runner
docker exec gitlab_runner_$GLUSER gitlab-runner register \
  --non-interactive                              \
  --name="Gitlab runner $GLUSER"                 \
  --url="https://gitlab.com/"                    \
  --token="$TOKEN"                               \
  --executor="docker"                            \
  --docker-image="debian:buster"                 \
  --docker-network-mode="test_network"