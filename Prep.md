# Lab Preparation

Lab preparation requires following steps to be executed.

## Install Requirements

Requirements on the machine to support execution of the lab:
* docker version 20.10.23, build 6051f14
* docker-compose version 1.29.2
* git version 2.34.1

## Create service-as-code basic container

Go to docker directory and run:

```
sudo docker login registry.gitlab.com
sudo docker build -t registry.gitlab.com/nso-service-as-code/service-as-code .
sudo docker push registry.gitlab.com/nso-service-as-code/service-as-code
```

## Readme

Click to go to [Readme Guide](Readme.md)