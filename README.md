# NSO Service as a Code

In this example we will demonstrate how to maintain desired network configuration in source code repository and use NSO to provision network using this definition

Operations' intention is stored in <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/blob/devdays/SaaC/service.yaml" target="_blank" rel="noopener noreferrer">yaml service definition files</a> Any changes to those files will trigger a CICD pipeline. Non-main branches trigger execution on the gitlab runners connected with netsim environments. Promotion of changes into main can provision the changes into the staging environment  

### Pipeline Definition

Pipeline stages are described in <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/blob/devdays/.gitlab-ci.yml" target="_blank" rel="noopener noreferrer">pipeline definition</a>

Provisioning stages trigger <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/blob/devdays/scripts/provision.py" target="_blank" rel="noopener noreferrer">provision.py</a> which performs following steps:

1. Retrieves all currently provisioned customers with this service
2. Reads each service file (i.e. service.yaml) and pushes the service data using a RESTCONF which triggers NSO to derive all the required changes and implements them in the network devices.
3. Executes test suite as defined in the robot test cases in <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/blob/devdays/SaaC/tests/robot.robot" target="_blank" rel="noopener noreferrer">robot</a>
4. Sends notification with test execution details to the Webex teams

## Setup

Initial steps to be followed to set things up

### Pull images and launch gitlab runner

```
./image-pull.sh
```

```
./setup.sh -u devdays
```

### Gitlab Runner Review

Check if your gitlab runner have been registered <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/settings/ci_cd" target="_blank" rel="noopener noreferrer">CI CD Settings</a>.


### Notification

Let's send the notifications to the webex room via <a href="https://developer.webex.com/my-apps/nso-service-as-code" target="_blank" rel="noopener noreferrer">NSO Service as Code bot</a> Once created bot has an access token and we can define it as cicd variable WEBEX_BOT_TOKEN in <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/settings/ci_cd" target="_blank" rel="noopener noreferrer">CI CD Settings</a>

This bot can be now added into our webex space. And finally we want to send notifications to webex room so we need to get out <a href="https://developer.webex.com/docs/api/v1/rooms/list-rooms" target="_blank" rel="noopener noreferrer">Room ID</a>

```
notify:
  # can specify room_id and/or WebexTeams person email
  room_id: Y2lzY29zcGFyazovL3VzL1JPT00vN2M0ZTQ2YjAtMTc3Mi0xMWVmLWFhMDMtMDMxODk3OTRjOWUz

```

1. Update room ID in  <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/blob/devdays/scripts/config.yaml" target="_blank" rel="noopener noreferrer">Config YAML</a>

2. Review <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/pipelines" target="_blank" rel="noopener noreferrer">pipeline execution</a>
## Operation's intention

### Deploy Service Intention

1. Edit Service <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/blob/devdays/SaaC/service.yaml" target="_blank" rel="noopener noreferrer">definition</a>

2. Add customer definition

```
customer-id: my_first_customer
sample:
  - name: first_service
    access-device:
      device-id: ios-0
      interface-id: 0/0/0/1
      ip-address: 10.0.1.2
      instance-id: 100
      vlan-id: 100
    delivery-device:
      device-id: ios-3
      interface-id: 0/0/1/1
      ip-address: 10.0.1.1
      instance-id: 100
      vlan-id: 100
    link:
      vc-id: 10000
      vc-class: 2
      qos-policy_name: policy
```

3. Review <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/pipelines" target="_blank" rel="noopener noreferrer">pipeline execution</a>

### Deploy Additional Service


1. Edit Service <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/blob/devdays/SaaC/service.yaml" target="_blank" rel="noopener noreferrer">definition</a>

2. Add customer definition

```
customer-id: my_first_customer
sample:
  - name: first_service
    access-device:
      device-id: ios-0
      interface-id: 0/0/0/1
      ip-address: 10.0.1.2
      instance-id: 100
      vlan-id: 100
    delivery-device:
      device-id: ios-3
      interface-id: 0/0/1/1
      ip-address: 10.0.1.1
      instance-id: 100
      vlan-id: 100
    link:
      vc-id: 10000
      vc-class: 2
      qos-policy_name: policy
  - name: second_service
    access-device:
      device-id: ios-1
      interface-id: 0/0/0/2
      ip-address: 10.0.2.2
      instance-id: 200
      vlan-id: 200
    delivery-device:
      device-id: ios-2
      interface-id: 0/0/0/2
      ip-address: 10.0.2.1
      instance-id: 200
      vlan-id: 200
    link:
      vc-id: 20000
      vc-class: 2
      qos-policy_name: policy
```

3. Review <a href="https://gitlab.com/nso-service-as-code/service-as-code/-/pipelines" target="_blank" rel="noopener noreferrer">pipeline execution</a>