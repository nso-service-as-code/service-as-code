#!/usr/bin/env python
import argparse
import os
import sys

import yaml
from attrdict import AttrDict
from webexteamssdk import WebexTeamsAPI


class Notify(object):
    """
    send webex notification to persons or teams
    """

    def __init__(self, config_file=None):
        # read config files
        if config_file is None:
            config_file = os.path.join(os.path.dirname(__file__), "config.yaml")
        with open(config_file) as fd:
            self.config = AttrDict(yaml.safe_load(fd.read()))

        if not hasattr(self.config, "notify"):
            raise ValueError("missing notify config section")

        if hasattr(self.config.notify, "token"):
            token = self.config.notify.token
        else:
            token = os.environ.get("WEBEX_BOT_TOKEN")
        if not token:
            raise ValueError("Err: no token configured in $WEBEX_BOT_TOKEN environment")

        # login to Webex
        self.api = WebexTeamsAPI(access_token=token)

    def notify(self, message, roomid=None):
        """
        send notification to room identified in the config file
        """
        if roomid is None:
            if hasattr(self.config.notify, "room_id"):
                roomid = self.config.notify.room_id
            else:
                roomid = None

        if roomid:
            self.api.messages.create(markdown=message, roomId=roomid)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Notify")
    parser.add_argument("--room", help="WebexTeams room id to send to")
    parser.add_argument("--config", help="config file to use")
    parser.add_argument("message", nargs=argparse.REMAINDER, help="message to be sent")
    args = parser.parse_args()

    if not len(args.message):
        print("no message provided")
        sys.exit(1)

    notif = Notify(config_file=args.config)
    message = (" ".join(args.message)).replace("\\n", "\n")
    print(message)
    notif.notify(message, roomid=args.room)
