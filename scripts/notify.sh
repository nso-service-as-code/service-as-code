#!/bin/bash

FAILED='❌'
PASSED='✅'
PENDING='⌛'

BRANCH=$1
PIPELINEURL=$2
STATUS=$3
TEST_JOB=$4

case "$STATUS" in
  success)  icon=$PASSED ;;
  failed|canceled)  icon=$FAILED ;;
  *)  echo "skipped notification"; exit 0;;
esac

message="""$icon Network Configuration **$STATUS** in environment **$BRANCH** \nPipeline  - <${PIPELINEURL}>\nTest Case - <${TEST_JOB}>"""

python ./scripts/notify.py $message
